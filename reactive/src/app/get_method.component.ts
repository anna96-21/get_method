import { Component } from '@angular/core';
import { DataGetService } from './data_get.service';

@Component({
    selector: 'get-method',
    templateUrl: './get_method.component.html',
    styleUrls: ['./get_method.component.css']
})

export class GetMethodComponent{
    photos: any[];

    constructor(public dataService: DataGetService){
        this.dataService.getPhotos().subscribe(
            photos => {
                this.photos = photos;
            }
        )    
    }
}
