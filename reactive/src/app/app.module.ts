import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { GetMethodComponent } from './get_method.component';
import { DataGetService } from './data_get.service';

@NgModule({
  declarations: [
    AppComponent,
    GetMethodComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [DataGetService],
  bootstrap: [AppComponent]
})
export class AppModule { }
